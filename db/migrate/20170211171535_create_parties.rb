class CreateParties < ActiveRecord::Migration[5.0]
  def type_to_sql(type, limit = nil, precision = nil, scale = nil)
    return super unless type.to_s == 'integer'

    case limit
      when 1; 'tinyint'
      when 2; 'smallint'
      when 3; 'mediumint'
      when nil, 4, 11; 'int(11)'  # compatibility with MySQL default
      when 5..8; 'bigint'
      else raise(ActiveRecordError, "No integer type has byte size #{limit}")
    end
  end

  def change
    create_table :parties do |t|
      t.string :host_name, :limit => 255, :default => nil
      t.string :host_email, :limit => 255, :default => nil
      t.integer :numgsts, :limit => 6, :precision => 0, :scale => 0
      t.text :guest_names
      t.string :venue, :limit => 255, :default => nil
      t.string :location, :limit => 255, :default => nil
      t.string :theme, :limit => 255, :default => nil
      t.datetime :when, :default => nil
      t.datetime :when_its_over, :default => nil
      t.text :descript, :default => nil
      t.timestamps
    end
  end
end
